FactoryGirl.define do
  factory :post do
    title 'New Post'
  end

  factory :post_image do
    name 'Sky'
    image '53e94f56-63ad-49fc-a0e4-8202c8fe58b9/Dafty.png'
    post
  end
end
