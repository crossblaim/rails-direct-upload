class PostImageProcessorJob < ApplicationJob
  attr_accessor :post_image_id
  queue_as :default

  def perform(post_image_id)
    self.post_image_id = post_image_id
    process
  end

  def process
    post_image = PostImage.find(post_image_id)
    post_image.update_attributes(
      remote_image_url: post_image.image.direct_fog_url(with_path: true),
      processed: true
    )
  end
end
