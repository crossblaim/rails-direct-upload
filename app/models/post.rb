class Post < ApplicationRecord
  has_many :post_images

  validates_presence_of :title
end
