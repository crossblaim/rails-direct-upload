class PostImage < ApplicationRecord
  mount_uploader :image, PostImageUploader

  belongs_to :post

  validates_presence_of :post, :name
end
